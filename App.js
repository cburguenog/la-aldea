import React from 'react';
import { StyleSheet, Text, View,Navigator, TouchableHighlight } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';


import LoginComponent from "./src/components/loginComponent";
import AcercaDeComponent from "./src/components/acercaDe";
import RegistroComponent from "./src/components/registroComponent";
import MainViewComponent from "./src/components/mainViewComponent";
import PlaylistComponent from "./src/components/playlistComponent";

const RootStack = createStackNavigator( {
    Login: LoginComponent,
    AcercaDe: AcercaDeComponent,
    Registro: RegistroComponent,
    Menu: MainViewComponent,
    Audiolibros:PlaylistComponent
});

const AppContainer = createAppContainer(RootStack);

export default AppContainer;