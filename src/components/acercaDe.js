import React from 'react';
import { StyleSheet, Text, View, Alert,KeyboardAvoidingView,TextInput,TouchableHighlight, SafeAreaView, ScrollView } from 'react-native';
import Constants from 'expo-constants';

export default class AcercaDeComponent extends React.Component {

    constructor(props){
        super(props);
    }

    back = ()=>{
        this.props.navigation.goBack();
    }


  render() {
    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.scrollView}>
                <Text style={styles.titulo} >La Aldea</Text>
                <Text style={styles.subtitulo} >Acerca de</Text>
                <Text style={styles.parrafo} >
                    La Aldea es una estrategia pedagógica que busca formar en ciudadanía y
                    valores a niños entre los 7 y los 14 años. En este universo de aprendizaje,
                    basado en historias de un grupo de animales que convive y que se en-
                    frenta a situaciones que son metáforas de la vida real, los niños se divier-
                    ten mientras reexionan sobre el mundo que los rodea. Este material pedagógico permite que los niños hagan un paralelo entre lo que sucede
                    en La Aldea, su vida cotidiana, y de manera más amplia, su comunidad y
                    su país.
                    {"\n"}
                    {"\n"}
                    Así como nosotros, los tapires, tortugas, camaleones y otros animales de
                    La Aldea sienten miedo, felicidad, vergüenza, entusiasmo y melancolía.
                    Cada una de las situaciones que suceden en esta comunidad es una
                    oportunidad para pensar sobre las emociones, la manera que sentimos y
                    la forma en la que nos relacionamos con nosotros mismos y lo que nos
                    rodea.
                    {"\n"}
                    {"\n"}
                    De esta manera, las historias y personajes de la Aldea se convierten en un
                    trampolín para que los niños fortalezcan sus habilidades de empatía, res-
                    olución de conictos, toma de decisiones y manejo de emociones y así
                    puedan contribuir a cambiar sus entornos de forma positiva. Además, es
                    una estrategia que permite acercarlos a las noticias del país, al hacerlos
                    reexionar sobre temas que incluyen los bienes públicos, la corrupción,
                    las elecciones, la integración social y el sistema de salud entre otros.
                </Text>

                <TouchableHighlight
                    style={styles.buttonBack}
                    onPress={this.back}
                >
                    <Text style={styles.buttonLabel} >Volver</Text>
                </TouchableHighlight>
            </ScrollView>
        </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    titulo:{
        marginTop:10,
        textAlign:"center",
        fontSize:50,
        fontWeight:"bold"
    },
    subtitulo:{
        marginTop:10,
        marginBottom:30,
        textAlign:"center",
        fontSize:30
    },
    scrollView: {
        marginHorizontal: 20,
        width:"90%"
    },
    parrafo:{
        marginBottom:50
    },
    buttonBack:{
        width:100,
        marginTop:10,
        backgroundColor:"#007bff",
        padding:10,
        alignItems:"center",
        alignSelf:"center",
        marginBottom:50
    }
});