import React from 'react';
import { StyleSheet, Text, View, Alert,Image,TextInput,TouchableHighlight, SafeAreaView, ScrollView } from 'react-native';
import Constants from 'expo-constants';
import * as axios from 'axios';

export default class MainViewComponent extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            username: '',
            email:'',
            name:'',
            lastname:'',
            city:''
        };

    }

    goAudioLibros = ()=>
    {
        this.props.navigation.navigate('Audiolibros');
    }

    goAcercaDe = ()=>
    {
        this.props.navigation.navigate('AcercaDe');
    }
    
    componentDidMount()
    {
        /*
        fetch("https://laaldea.co/apilaaldeadev/get_nonce/?controller=user&method=register")
        .then( (respuesta) =>{
            const respuestaJson = respuesta.json();
            Alert.alert("todo ok",`${JSON.stringify(respuestaJson)}`)
            
            if(respuestaJson.status === "ok")
            {
                Alert.alert("todo ok",`${respuestaJson.nonce}`)
            }else
            {
                Alert.alert("Error",`No se puede registrar en estos momentos`);
            }
        })
        .catch( (error) =>{ Alert.alert("Error",JSON.stringify(error)); })
        */
    }


  render() {
    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.scrollView}>
                <Text style={styles.titulo} >La Aldea</Text>
                <View style={styles.btnWrapper} >
                    <View style={styles.buttonContainer}>
                        <TouchableHighlight
                            style={styles.btnAudiolibros}
                            onPress={this.goAudioLibros}
                        >
                            <Text style={styles.buttonLabel} >Audiolibros</Text>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.buttonContainer}>
                        <TouchableHighlight
                            style={styles.buttonAbout}
                            onPress={this.goAcercaDe}
                        >
                            <Text style={styles.buttonLabel} >Acerda de</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        backgroundColor: '#ecf0f1',
        alignItems:"center",
        marginTop: Constants.statusBarHeight,
        marginHorizontal: 16,
    },
    btnWrapper:{
        flex:1,
        flexDirection:"row",
        alignItems: 'center',
        justifyContent: 'center',
        marginTop:15
    },
    buttonContainer:{
        flex:1
    },
    titulo:{
        marginTop:10,
        marginBottom:30,
        textAlign:"center",
        fontSize:30,
        fontWeight:"bold"
    },
    input:{
        width:300,
        height:44,
        padding:10,
        borderWidth:1,
        borderColor:'black',
        marginBottom:15
    },
    button:{
        width:200,
        marginTop:10,
        backgroundColor:"#007bff",
        padding:10,
        alignItems:"center",
        alignSelf:"center"
    },
    btnAudiolibros:{
        width:150,
        height:250,
        marginTop:10,
        backgroundColor:"#007bff",
        padding:10,
        alignItems:"center",
        alignSelf:"flex-start",
    },
    buttonAbout:{
        width:150,
        height:250,
        marginTop:10,
        backgroundColor:"#007bff",
        padding:10,
        alignItems:"center",
        alignSelf:"flex-end",
        marginHorizontal:5
    },
    buttonLabel:{
        color:"#fff"
    },
    keyboardViewContainer: {
        width: '100%', 
        alignItems: 'center'
    },
    scrollView: {
        marginHorizontal: 20,
    },
    facebook_btn:{
        marginTop:15
    }
});