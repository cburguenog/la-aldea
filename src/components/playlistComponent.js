import React from 'react';
import { StyleSheet, Text, View, Alert,TextInput,Image,TouchableHighlight, SafeAreaView, ScrollView } from 'react-native';
import Constants from 'expo-constants';
import { Audio } from 'expo';

const thumnail = require('../assets/images/placeholder.jpg')

export default class PlaylistComponent extends React.Component {

    constructor(props){
        super(props);
        this.playbackInstance = null;
    }
    
    componentDidMount() {
        Audio.setAudioModeAsync({
                allowsRecordingIOS: false,
                interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
                playsInSilentModeIOS: true,
                shouldDuckAndroid: true,
                interruptionModeAndroid:          Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
                playThroughEarpieceAndroid: false,
            });
        //  This function will be called
            this._loadNewPlaybackInstance(true);
    }

    async _loadNewPlaybackInstance(playing) {
        if (this.playbackInstance != null) {
            await this.playbackInstance.unloadAsync();
            this.playbackInstance.setOnPlaybackStatusUpdate(null);
            this.playbackInstance = null;
         }
         const source = require('../assets/audiobooks/1-Los-favores-de-Arnulfo.mp3');
         const initialStatus = {
    //        Play by default
              shouldPlay: true,
    //        Control the speed
              rate: 1.0,
    //        Correct the pitch
              shouldCorrectPitch: true,
    //        Control the Volume
              volume: 1.0,
    //        mute the Audio
              isMuted: false
         };
         const { sound, status } = await Audio.Sound.createAsync(
             source,
             initialStatus
        );
    //  Save the response of sound in playbackInstance
        this.playbackInstance = sound;
    //  Make the loop of Audio
        this.playbackInstance.setIsLoopingAsync(true);
    //  Play the Music
        this.playbackInstance.playAsync();
    }

    componentWillUnmount() {
        this.playbackInstance.unloadAsync();
    }


  render() {
    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.scrollView}>
                <View style={styles.wrapperItem} >
                    <Image style={styles.thumbnail} source={thumnail}/>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        backgroundColor: '#ecf0f1',
        alignItems:"flex-start",
        marginTop: Constants.statusBarHeight,
        marginHorizontal: 16,
    },
    scrollView: {
        marginHorizontal: 20,
    },
    thumbnail:{
        width: 80, 
        height:80,
        alignSelf:"flex-start"
    }
});