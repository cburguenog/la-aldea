import React from 'react';
import { StyleSheet, Text, View, Alert,KeyboardAvoidingView,TextInput,TouchableHighlight, SafeAreaView, ScrollView,Image, AsyncStorage  } from 'react-native';
import Constants from 'expo-constants';
import * as axios from 'axios';

const facebookLogin = require('../assets/images/facebook_btn.png')

export default class LoginComponent extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            username: '',
            password: '',
            isValid:false
        };
    }

    onLogin = async () => {
        const { username,password } = this.state;

        if( username && password )
        {
            this.props.navigation.push('Menu')
            /*
            let formData = new FormData();
            formData.append('username', username);
            formData.append('password', password);

            axios.post("https://laaldea.co/apilaaldeadev/auth/generate_auth_cookie",{formData})
            .then((response) => response.json())
            .then((responseJson) => { Alert.alert("resultado",JSON.stringify(responseJson))  })
            .catch((error) => {
                Alert.alert("error",JSON.stringify(error))
            });
            */
        }else
        {
            Alert.alert('Error',`Favor de completar el formulario para continuar.`); 
        }
    }

    /**
	 * Store auth credentials to device.
	 *
	 */
	async saveToStorage(userData){
		if (userData) {
			await AsyncStorage.setItem('user', JSON.stringify({
					isLoggedIn: true,
					authToken: userData.auth_token,
					id: userData.user_id,
					name: userData.user_login
				})
			);
			return true;
		}

		return false;
	}

    goAcercaDe = ()=>
    {
        this.props.navigation.navigate('AcercaDe');
    }

    goRegistro = ()=>
    {
        this.props.navigation.navigate('Registro');
    }


  render() {
    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.scrollView}>
                <Text style={styles.titulo} >La Aldea</Text>
                <TextInput
                    value={this.state.username}
                    onChangeText={(username)=>{this.setState({username})}}
                    placeholder={"email@ejemplo.com"}
                    textContentType="emailAddress"
                    style={styles.input}
                />
                <TextInput
                    value={this.state.password}
                    onChangeText={(password)=>{this.setState({password})}}
                    placeholder={"*****************"}
                    secureTextEntry={true}
                    style={styles.input}
                />
                <TouchableHighlight
                 style={styles.button}
                 onPress={this.onLogin}
                >
                    <Text style={styles.buttonLabel} >Login</Text>
                </TouchableHighlight>

                <TouchableHighlight>
                    <Image style={styles.facebook_btn} source={facebookLogin} />
                </TouchableHighlight>
                <View style={styles.btnWrapper} >
                    <View style={styles.buttonContainer}>
                        <TouchableHighlight
                            style={styles.buttonRegistro}
                            onPress={this.goRegistro}
                        >
                            <Text style={styles.buttonLabel} >Registro</Text>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.buttonContainer}>
                        <TouchableHighlight
                            style={styles.buttonAbout}
                            onPress={this.goAcercaDe}
                        >
                            <Text style={styles.buttonLabel} >Acerda de</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        backgroundColor: '#ecf0f1',
        alignItems:"center",
        marginTop: Constants.statusBarHeight,
        marginHorizontal: 16,
    },
    btnWrapper:{
        flex:1,
        flexDirection:"row",
        alignItems: 'center',
        justifyContent: 'center',
        marginTop:15
    },
    buttonContainer:{
        flex:1
    },
    titulo:{
        marginTop:10,
        marginBottom:30,
        textAlign:"center",
        fontSize:50,
        fontWeight:"bold"
    },
    input:{
        width:300,
        height:44,
        padding:10,
        borderWidth:1,
        borderColor:'black',
        marginBottom:10
    },
    button:{
        width:200,
        marginTop:10,
        backgroundColor:"#007bff",
        padding:10,
        alignItems:"center",
        alignSelf:"center"
    },
    buttonRegistro:{
        width:100,
        marginTop:10,
        backgroundColor:"#007bff",
        padding:10,
        alignItems:"center",
        alignSelf:"flex-start"
    },
    buttonAbout:{
        width:100,
        marginTop:10,
        backgroundColor:"#007bff",
        padding:10,
        alignItems:"center",
        alignSelf:"flex-end",
        marginHorizontal:10
    },
    buttonLabel:{
        color:"#fff"
    },
    keyboardViewContainer: {
        width: '100%', 
        alignItems: 'center'
    },
    scrollView: {
        marginHorizontal: 20,
    },
    facebook_btn:{
        marginTop:15
    }
});