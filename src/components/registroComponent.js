import React from 'react';
import { StyleSheet, Text, View, Alert,KeyboardAvoidingView,TextInput,TouchableHighlight, SafeAreaView, ScrollView } from 'react-native';
import Constants from 'expo-constants';
import * as axios from 'axios';

export default class RegistroComponent extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            username: '',
            email:'',
            name:'',
            lastname:'',
            city:''
        };

    }

    componentDidMount()
    {
        /*
        fetch("https://laaldea.co/apilaaldeadev/get_nonce/?controller=user&method=register")
        .then( (respuesta) =>{
            const respuestaJson = respuesta.json();
            Alert.alert("todo ok",`${JSON.stringify(respuestaJson)}`)
            
            if(respuestaJson.status === "ok")
            {
                Alert.alert("todo ok",`${respuestaJson.nonce}`)
            }else
            {
                Alert.alert("Error",`No se puede registrar en estos momentos`);
            }
        })
        .catch( (error) =>{ Alert.alert("Error",JSON.stringify(error)); })
        */
    }

    onRegistrer = ()=>{
        Alert.alert("Aviso","No se logro registrar al usuario");
    }

    back = ()=>{
        this.props.navigation.goBack();
    }


  render() {
    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.scrollView}>
                <Text style={styles.titulo} >Registro</Text>
                <TextInput
                    value={this.state.username}
                    onChangeText={(username)=>{this.setState({username})}}
                    placeholder={"Mi usuario"}
                    style={styles.input}
                />
                <TextInput
                    value={this.state.email}
                    onChangeText={(email)=>{this.setState({email})}}
                    placeholder={"email@ejemplo.com"}
                    style={styles.input}
                />
                <TextInput
                    value={this.state.name}
                    onChangeText={(name)=>{this.setState({name})}}
                    placeholder={"Nombre"}
                    style={styles.input}
                />
                <TextInput
                    value={this.state.lastname}
                    onChangeText={(lastname)=>{this.setState({lastname})}}
                    placeholder={"Apellidos"}
                    style={styles.input}
                />
                <TouchableHighlight
                 style={styles.button}
                 onPress={this.onRegistrer}
                >
                    <Text style={styles.buttonLabel} >Registrar</Text>
                </TouchableHighlight>

                <TouchableHighlight
                 style={styles.button}
                 onPress={this.back}
                >
                    <Text style={styles.buttonLabel} >Volver</Text>
                </TouchableHighlight>
            </ScrollView>
        </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        backgroundColor: '#ecf0f1',
        alignItems:"center",
        marginTop: Constants.statusBarHeight,
        marginHorizontal: 16,
    },
    btnWrapper:{
        flex:1,
        flexDirection:"row",
        alignItems: 'center',
        justifyContent: 'center',
        marginTop:15
    },
    buttonContainer:{
        flex:1
    },
    titulo:{
        marginTop:10,
        marginBottom:30,
        textAlign:"center",
        fontSize:30,
        fontWeight:"bold"
    },
    input:{
        width:300,
        height:44,
        padding:10,
        borderWidth:1,
        borderColor:'black',
        marginBottom:15
    },
    button:{
        width:200,
        marginTop:10,
        backgroundColor:"#007bff",
        padding:10,
        alignItems:"center",
        alignSelf:"center"
    },
    buttonRegistro:{
        width:100,
        marginTop:10,
        backgroundColor:"#007bff",
        padding:10,
        alignItems:"center",
        alignSelf:"flex-start"
    },
    buttonAbout:{
        width:100,
        marginTop:10,
        backgroundColor:"#007bff",
        padding:10,
        alignItems:"center",
        alignSelf:"flex-end",
        marginHorizontal:10
    },
    buttonLabel:{
        color:"#fff"
    },
    keyboardViewContainer: {
        width: '100%', 
        alignItems: 'center'
    },
    scrollView: {
        marginHorizontal: 20,
    },
    facebook_btn:{
        marginTop:15
    }
});